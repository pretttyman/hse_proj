var BEST_TEMP = 26;

function day_mark(day, BOUNDS) {
    var best = BOUNDS.best;
    var worst = BOUNDS.worst;
    var temp = day.temp;
    var clouds = day.clouds;
    var wind = day.wind;
    var temp_mark = 1 - (Math.abs(BEST_TEMP - temp) - best.temp_deviation) / (worst.temp_deviation - best.temp_deviation);
    var clouds_mark = 1 - (day.clouds - best.clouds) / (worst.clouds - best.clouds);
    var wind_mark = 1 - (day.wind - best.wind) / (worst.wind - best.wind);
    return temp_mark * 0.5 + clouds_mark * 0.4 + wind_mark * 0.1;
}

module.exports = day_mark;