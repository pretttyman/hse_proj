function setMapControls(map) {
    document.getElementById('hybrid-button').onclick = () => {
        map.setMapTypeId(google.maps.MapTypeId.HYBRID);
    }
    document.getElementById('scheme-button').onclick = () => {
        map.setMapTypeId(google.maps.MapTypeId.TERRAIN);
    }
    document.getElementById('zoom-increase-btn').onclick = () => {
        map.setZoom(map.zoom + 1);
    }
    document.getElementById('zoom-decrease-btn').onclick = () => {
        map.setZoom(map.zoom - 1);
    }
    document.getElementById('world-button').onclick = () => {
        map.setZoom(3);
        map.setCenter({lat: 15, lng: 10});
    }
    window.onresize = () => {
        var mapDiv = document.getElementById('googleMap');
        var rating = document.getElementById('rating');
        if (!rating.classList.contains('rating_hidden')) {
            mapDiv.style.height = document.body.offsetHeight - rating.offsetHeight + 'px';
        }
        else {
            mapDiv.style.height = document.body.offsetHeight + 'px';
        }
        google.maps.event.trigger(map, "resize");
    };
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            document.getElementById('location-button').onclick = () => {
                map.setCenter(pos);
                map.setZoom(6);
            }
        }, () => {
            alert('Не удалось определить местонахождение, попробуйте в другом браузере :(');
            document.getElementById('location-button').remove();
        });
    } else {
        document.getElementById('location-button').remove();
    }
}

function createMap() {
    var map_config = {
        zoom: 4,
        center: {lat: 41.85, lng: -87.65},
        disableDefaultUI: true,
        streetViewControl: true,
        mapTypeId: google.maps.MapTypeId.TERRAIN
    }
    var map = new google.maps.Map(document.getElementById('googleMap'), map_config);
    var map_styles = [{
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [{ "visibility": "off" }]
    }];
    map.setOptions({styles: map_styles});
    setMapControls(map);
    return map;
}

module.exports = createMap;