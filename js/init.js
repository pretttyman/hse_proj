require('./../css/main.css');
require('./../css/map.css');
require('./../css/rating.css');
require('./../css/calendar.css');

var initCalendarModule = require('./show_calendar.js');
var initRatingModule = require('./show_rating.js');
var createMap = require('./map.js');
var map = createMap();
var chosen_date = {};
var ratingModule = initRatingModule(map, chosen_date);
var calendarModule = initCalendarModule(chosen_date);

function setEvents() {
    document.getElementById('header__open-calendar').onclick = calendarModule.toggleCalendar;
    document.getElementById('calendar-layer').onclick = calendarModule.hideCalendar;
    document.getElementsByClassName('rating__toggle-button')[0].onclick = ratingModule.rateToggle;
    document.getElementById('rating').addEventListener('transitionend', (event) => {
        event.stopPropagation();
        var rating = document.getElementById('rating');
        if (!rating.classList.contains('rating_hidden')) {
            var map_div = document.getElementById('googleMap');
            map_div.style.height = document.body.offsetHeight - rating.offsetHeight + 'px';
            google.maps.event.trigger(map, "resize");
        }
    }, true);
    document.getElementById('header__search-button').onclick = () => {
        calendarModule.hideCalendar();
        ratingModule.getTop(map);
    }
}

function init() {
    var calendar = new calendarModule.Calendar();
    setEvents();
    calendarModule.toggleCalendar();
}

window.onload = init;