var make_rating = require('./rating.js');
var data = require('./load_cities.js');

module.exports = (map, chosen_date) => {
    var markers = [];
        
    function getTop() {
        if (chosen_date.first === undefined || chosen_date.second === undefined) {
            return alert("Не выбрана дата");
        }
        if (chosen_date.first.getDate() > chosen_date.second.getDate()) {
            var t = chosen_date.first;
            chosen_date.first = chosen_date.second;
            chosen_date.second = t;
        }
        var rate = make_rating({begin: chosen_date.first.getDate(), end: chosen_date.second.getDate()});
        var rating = document.getElementById("top");
        rating.innerHTML = "";
        markers.forEach((marker) => {
            marker.setMap(null);
        })
        for (var i = 0; i < 10; i++) {
            rating.appendChild(createCityDiv(rate[i], i));
            var marker = createMarker({lat: rate[i].lat, lng: rate[i].lng});
            marker.setMap(map);
            markers[i] = marker;
        }
        map.setZoom(2);
        map.setCenter({lat: 0, lng: 10});
        var top_block = document.getElementById("rating");
        var mapDiv = document.getElementById('googleMap');
        if (top_block.classList.contains('rating_hidden')) {
            rateToggle();
        }
    }
    
    function createMarker(city_pos) {
        var marker = new google.maps.Marker({
            position: city_pos,
            animation: google.maps.Animation.DROP
        });
        google.maps.event.addListener(marker,'click', function() {
            map.setZoom(map.zoom + 1);
            map.setCenter(marker.getPosition());
        });
        
        return marker;
    }

    function getAverange(weather) {
        var clouds = 0,
            temp = 0,
            wind = 0;
            
        for (var cur = chosen_date.first.getDate(); cur <= chosen_date.second.getDate(); cur++) {
            temp += weather[cur].temp;
            clouds += weather[cur].clouds;
            wind += weather[cur].wind;  
        }
        
        var cnt = (chosen_date.second.getDate() - chosen_date.first.getDate() + 1);
        temp /= cnt;
        clouds /= cnt;
        wind /= cnt;
        
        return {
            temp: temp,
            clouds: clouds,
            wind: wind
        }
    }

    function createIcon(averange) {
        var icon = document.createElement('img');
        icon.classList.add('city__icon');
        
        var temp = averange.temp,
            clouds = averange.clouds,
            wind = averange.wind;
            
        if (temp > 24 && clouds < 10 || temp > 28 && clouds < 40) {
            icon.src = './pic/weather/sunny0.png';
        }
        else if (clouds < 15) {
            icon.src = './pic/weather/sunny1.png';
        }
        else if (clouds < 20) {
            icon.src = './pic/weather/sunny2.png';
        }
        else {
            icon.src = './pic/weather/sunny3.png';
        }
        return icon;
    }

    function weatherDiv(averange) {
        return `<div class='city__weather'>
            <img class='city__weather__icon' src='./pic/weather/temp.png'>
            <p> ${Math.round(averange.temp)}° </p>
        </div>`;
    }

    function createCityDiv(city_data, rank) {
        var city = document.createElement('div');
        var averange = getAverange(data[city_data.num].weather);
        city.appendChild(createIcon(averange));
        city.dataset.rank = rank;
        city.className = 'city';
        city.innerHTML += `<h3 class='city__name'> ${rank + 1}. ${city_data.city}, ${city_data.country} </h3>` + weatherDiv(averange);
        city.addEventListener('click', clickOnCity.bind(city), true);
        city.addEventListener('mouseenter', mouseEnterCity.bind(city), false);
        city.addEventListener('mouseleave', mouseLeaveCity.bind(city), false);
        return city; 
    }
    
    function mouseEnterCity(event) {
        event.stopPropagation();
        markers[this.dataset.rank].setAnimation(google.maps.Animation.BOUNCE);
    }
    
    function mouseLeaveCity(event) {
        event.stopPropagation();
        markers[this.dataset.rank].setAnimation(null);
    }
    
    function clickOnCity(event) {
        var rank = this.dataset.rank;
        var marker = markers[Number(rank)];
        event.stopPropagation();
        map.setCenter(markers[Number(rank)].position);
        map.setZoom(7);
        var cities = document.getElementsByClassName('rating__city_chosen');
        [].forEach.call(cities, (city) => {
            city.classList.remove('rating__city_chosen');
        });
        this.classList.add('rating__city_chosen');
    }

    function rateToggle() {
        var mapDiv = document.getElementById('googleMap');
        var rate = document.getElementById('rating');
        if (rate.classList.contains('rating_hidden')) {
            rate.classList.remove('rating_hidden');
        }
        else {
            mapDiv.style.height = document.body.offsetHeight + 'px';
            google.maps.event.trigger(map, "resize");
            rate.classList.add('rating_hidden');
        }
    }
    
    return {
        rateToggle: rateToggle,
        getTop: getTop
    }
    
}

