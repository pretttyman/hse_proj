var months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
var months2 = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];


module.exports = (chosen_date) => {
    
    function createCalendarTable() {
        var tab = document.createElement('table');
        tab.classList.add('calendar__days');
        for (var i = 0; i < 6; i++) {
            tab.insertRow(0);
            for (var j = 0; j < 7; j++) {
                tab.rows[0].insertCell(0);
                tab.rows[0].cells[0].classList.add('calendar__day');
            }
        }
        return tab;
    }

    function insertWeekRow(tab) {
        var week = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'];
        var week_row = tab.insertRow(0);
        week.forEach((week_day, iter) => {
            var day = week_row.insertCell(iter);
            day.classList.add('calendar__week-day');
            day.textContent = week_day;
        });
    }

    function fillDays(tab, year, month) {
        var first1 = new Date(year, month, 1);
        var week_first = first1.getDay();
        var days_cnt = 32 - new Date(year, month, 32).getDate();
        var cur_row = 1;
        var week_day = week_first - 1;
        if (week_day === -1) {
            week_day = 6;
        }
        for (var day = 1; day <= days_cnt; day++) {
            if (week_day === 7) {
                cur_row++;
                week_day = 0;
            }
            var cell = tab.rows[cur_row].cells[week_day];
            var curdate = new Date(year, month, day);
            setDayDiv(cell, curdate);
            week_day++;
        }
    }

    function setDayDiv(day, date) {
        day.textContent = date.getDate();
        day.dataset.date = date;
        day.addEventListener('click', clickDay);
        day.addEventListener('mouseover', dayHover);
        day.addEventListener('mouseout', unlightAllDays);
        if (+chosen_date.first == +date || + chosen_date.second == +date) {
            day.classList.add('calendar__day_chosen');
        }
        if (+chosen_date.first < +date && +date < +chosen_date.second) {
            day.classList.add('calendar__day_between');
        }
    }

    function clickDay() {
        this.classList.add('calendar__day_chosen');
        if (chosen_date.first !== undefined) {
            if (chosen_date.second !== undefined) {
                chosen_date.first = undefined;
                chosen_date.second = undefined;
                unlightAllDays();
                unsetChosenDays();
                document.getElementById('header__open-calendar').children[0].textContent = 'Выбрать дату';
            }
            else {
                chosen_date.second = new Date(this.dataset.date);
                showPeriod();
                lightDaysBetween(chosen_date.first, chosen_date.second);   
            }
        }
        else {
            chosen_date.first = new Date(this.dataset.date);
        }
    }

    function dayHover() {
        var date = new Date(this.dataset.date);
        if (chosen_date.first !== undefined && chosen_date.second === undefined && +chosen_date.first !== +date) {
            lightDaysBetween(chosen_date.first, date);
        }
    }

    function lightDaysBetween(first, second) {
        if (first > second) {
            var tmp = second;
            second = first;
            first = tmp;
        }
        var all = document.getElementsByClassName('calendar__day');
        for (var i = 0; i < all.length; i++) {
            var date = new Date(all[i].dataset.date);
            if (first < date && date < second) {
                all[i].classList.add('calendar__day_between');
            }
        }
    }

    function unlightAllDays() {
        if (chosen_date.first !== undefined && chosen_date.second !== undefined) {
            return;
        }
        var all = document.getElementsByClassName('calendar__day');
        for (var i = 0; i < all.length; i++) {
        all[i].classList.remove('calendar__day_between');
        }
    }

    function unsetChosenDays() {
        var all = document.getElementsByClassName('calendar__day');
        for (var i = 0; i < all.length; i++) {
        all[i].classList.remove('calendar__day_chosen');
        }
    }

    function showPeriod() {
        if (chosen_date.first > chosen_date.second) {
            var t = chosen_date.first;
            chosen_date.first = chosen_date.second;
            chosen_date.second = t;
        }
        document.getElementById('header__open-calendar').children[0].textContent = chosen_date.first.getDate() + ' ' 
            + months2[chosen_date.first.getMonth()] + ' - ' + chosen_date.second.getDate() + ' ' + months2[chosen_date.second.getMonth()];
    }

    function create_calendar(year, month) {
        var calendar = document.createElement('div');
        calendar.classList.add('calendar');
        var Month = document.createElement('div');
        Month.classList.add('calendar__month');
        Month.textContent = months[month] + ' ' + year;
        var tab = createCalendarTable();
        insertWeekRow(tab);
        fillDays(tab, year, month);
        calendar.appendChild(Month);
        calendar.appendChild(tab);
        document.getElementById('calendars').appendChild(calendar);
        if (chosen_date.first !== undefined && chosen_date.second !== undefined) {
            lightDaysBetween(chosen_date.first, chosen_date.second);
        }
        return calendar;
    }

    class Calendar {
        constructor() {
            document.getElementsByClassName('calendars__left-button')[0].onclick = this.prevMonth.bind(this);
            document.getElementsByClassName('calendars__right-button')[0].onclick = this.nextMonth.bind(this);
            this.year = new Date().getFullYear();
            this.month = new Date().getMonth();
            this.init();
        }
        init() {
            var calendars = document.getElementById('calendars');
            var first_child = calendars.childNodes[0];
            calendars.insertBefore(this.new_(this.year, this.month - 1), first_child);
            calendars.insertBefore(this.new_(this.year, this.month), first_child);
            calendars.insertBefore(this.new_(this.year, this.month + 1), first_child);
            calendars.insertBefore(this.new_(this.year, this.month + 2), first_child);
            this.setClasses();
        }
        setClasses() {
            var calendars = document.getElementById('calendars').childNodes;
            calendars[0].classList.add('calendar_hidden');
            calendars[1].classList.add('calendar_left');
            calendars[2].classList.add('calendar_right');
            calendars[3].classList.add('calendar_hidden');
        }
        shiftRight() {
            var calendars = document.getElementById('calendars').childNodes;
            calendars[0].classList.add('calendar_hidden');
            calendars[1].classList.add('calendar_left');
            calendars[1].classList.remove('calendar_hidden');
            calendars[3].classList.remove('calendar_left');
            calendars[3].classList.add('calendar_right');
            this.animation = 2;
        }
        afterRightShift() {
            var calendars_block = document.getElementById('calendars');
            var calendars = calendars_block.childNodes;
            if (this.animation != 2) {
                return;
            }
            calendars[2].classList.remove('calendar_right');
            calendars[2].classList.add('calendar_hidden');
            calendars_block.insertBefore(calendars[3], calendars[2]);
            this.animation = 0;
        }
        prevMonth() {
            if (this.animation) {
                return;
            }
            this.month--;
            this.correctDate()
            var calendars = document.getElementById('calendars');
            calendars.removeChild(calendars.childNodes[3]);
            calendars.insertBefore(this.new_(this.year, this.month - 1), calendars.childNodes[0]);
            calendars.insertBefore(calendars.childNodes[3], calendars.childNodes[2]);
            this.shiftRight();
            calendars.childNodes[3].addEventListener('transitionend', this.afterRightShift.bind(this));
        }
        shiftLeft() {
            var calendars = document.getElementById('calendars').childNodes;
            calendars[1].classList.add('calendar_right');
            calendars[1].classList.remove('calendar_hidden');
            calendars[2].classList.remove('calendar_right');
            calendars[2].classList.add('calendar_left');
            calendars[3].classList.add('calendar_hidden');
            this.animation = 1;
        }
        afterLeftShift() {
            var calendars_block = document.getElementById('calendars');
            var calendars = calendars_block.childNodes;
            if (this.animation != 1) {
                return;
            }
            calendars[0].classList.add('calendar_hidden');
            calendars[0].classList.remove('calendar_left');
            calendars_block.insertBefore(calendars[2], calendars[1]);
            this.animation = 0;
        }
        nextMonth() {
            if (this.animation) {
                return;
            }
            this.month++;
            this.correctDate()
            var calendars = document.getElementById('calendars');
            calendars.removeChild(calendars.childNodes[0]);
            calendars.insertBefore(this.new_(this.year, this.month + 2), calendars.childNodes[3]);
            calendars.insertBefore(calendars.childNodes[2], calendars.childNodes[1]);
            this.shiftLeft();
            calendars.childNodes[2].addEventListener('transitionend', this.afterLeftShift.bind(this));
        }
        correctDate() {
            if (this.month < 0) {
                this.month += 12;
                this.year--;
            }
            if (this.month > 11) {
                this.month %= 12;
                this.year++;
            }
        }
        new_(year, month) {
            if (month < 0) {
                month = 12 + month;
                year--;
            }
            if (month > 11) {
                month %= 12;
                year++;
            }
            var new_c =  create_calendar(year, month);
            return new_c;
        }
    }

    function hideCalendar() {
        var calendars = document.getElementById('calendars');
        calendars.classList.add('calendars_hidden');
        document.getElementById('calendar-layer').style.display = 'none';
    }

    function showCalendar() {
        var calendars = document.getElementById('calendars');
        calendars.classList.remove('calendars_hidden');
        document.getElementById('calendar-layer').style.display = 'block';   
    }

    function toggleCalendar() {
        var calendars = document.getElementById('calendars');
        if (calendars.classList.contains('calendars_hidden')) {
            showCalendar();
        }
        else {
            hideCalendar();
        }
    }

    return {
        toggleCalendar: toggleCalendar,
        hideCalendar: hideCalendar,
        Calendar: Calendar
    }
}

