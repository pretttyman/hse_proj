var day_mark = require('./day_mark.js');
var data = require('./load_cities.js');

const inf = 1000;
const BEST_TEMP = 26;

function getBounds(period) {
    
    var max_temp_deviation = 0,
        min_temp_deviation = inf,
        max_wind = -inf,
        min_wind = inf,
        max_clouds = -inf,
        min_clouds = inf;
        
    data.forEach((city_data) => {
        city_data.weather.forEach((day) => {
            min_temp_deviation = Math.min(min_temp_deviation, Math.abs(day.temp - BEST_TEMP));
            max_temp_deviation = Math.max(max_temp_deviation, Math.abs(day.temp - BEST_TEMP));
            max_wind = Math.max(max_wind, day.wind);
            min_wind = Math.min(min_wind, day.wind);
            max_clouds = Math.max(max_clouds, day.clouds);
            min_clouds = Math.min(min_clouds, day.clouds);
        });
    });
    
    return {
        best: {temp_deviation: min_temp_deviation, wind: min_wind, clouds: min_clouds},
        worst: {temp_deviation: max_temp_deviation, wind: max_wind, clouds: max_clouds}
    }
}

function markCity(weather, period, bounds) {
    var mark = 0;
    for (var j = period.begin; j <= period.end; j++) {
        mark += day_mark(weather[j], bounds);
    }
    mark = mark / (period.end - period.begin + 1);
    return mark;
}

function make_rating(period) {
    var rate = [];
    var bounds = getBounds(period);
    
    data.forEach((city_data, iter) => {
        rate.push({
            city: city_data.city_name,
            country: city_data.country_name,
            lat: city_data.latt,
            lng: city_data.long,
            mark: markCity(city_data.weather, period, bounds),
            num: iter
        });
    });
    
    rate.sort((a, b) => a.mark < b.mark ? 1 : -1);
    return rate;
}

module.exports = make_rating;