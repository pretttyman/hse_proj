data = [{'city': 'Hong Kong', 'popularity2014': 27.77, 'geometry': [22.2793278, 114.1628131], 'country': 'China', 'popularity2013': 25.6611}, {'city': 'London', 'popularity2014': 17.3839, 'geometry': [51.5073219, -0.1276473], 'country': 'United Kingdom', 'popularity2013': 16.784}, {'city': 'Singapore', 'popularity2014': 17.0862, 'geometry': [1.2904527, 103.852038], 'country': 'Singapore', 'popularity2013': 17.1467}, {'city': 'Bangkok', 'popularity2014': 16.245, 'geometry': [13.5859219, 100.4160866], 'country': 'Thailand', 'popularity2013': 17.4678}, {'city': 'Paris', 'popularity2014': 14.9817, 'geometry': [48.8566101, 2.3514992], 'country': 'France', 'popularity2013': 15.2719}, {'city': 'Macau', 'popularity2014': 14.9665, 'geometry': [-5.1126695, -36.6352093], 'country': 'Macau', 'popularity2013': 13.9353}, {'city': 'Shenzhen', 'popularity2014': 13.1208, 'geometry': [22.5442673, 114.0545327], 'country': 'China', 'popularity2013': 12.1489}, {'city': 'New York City', 'popularity2014': 12.23, 'geometry': [40.7305991, -73.9865811], 'country': 'USA', 'popularity2013': 11.8504}, {'city': 'Istanbul', 'popularity2014': 11.8712, 'geometry': [41.0096334, 28.9651646], 'country': 'Turkey', 'popularity2013': 10.4863}, {'city': 'Kuala Lumpur', 'popularity2014': 11.6296, 'geometry': [3.1570976, 101.7009528], 'country': 'Malaysia', 'popularity2013': 11.1823}, {'city': 'Antalya', 'popularity2014': 11.4985, 'geometry': [36.9008783, 30.6937117], 'country': 'Turkey', 'popularity2013': 11.1207}, {'city': 'Dubai', 'popularity2014': 11.3873, 'geometry': [25.2683521, 55.2961962], 'country': 'United Arab Emirates', 'popularity2013': 10.4583}, {'city': 'Seoul', 'popularity2014': 9.3896, 'geometry': [37.5666791, 126.9782914], 'country': 'South Korea', 'popularity2013': 8.619}, {'city': 'Rome', 'popularity2014': 8.7805, 'geometry': [41.8935085, 12.4825526], 'country': 'Italy', 'popularity2013': 8.6083}, {'city': 'Taipei', 'popularity2014': 8.615, 'geometry': [25.0375167, 121.5637], 'country': 'Taiwan', 'popularity2013': 7.0041}, {'city': 'Guangzhou', 'popularity2014': 8.199, 'geometry': [23.1300037, 113.259001], 'country': 'China', 'popularity2013': 7.682}, {'city': 'Phuket', 'popularity2014': 8.1153, 'geometry': [7.8882251, 98.3914686], 'country': 'Thailand', 'popularity2013': 8.035}, {'city': 'Miami', 'popularity2014': 7.26, 'geometry': [25.7742658, -80.1936588], 'country': 'USA', 'popularity2013': 6.2751}, {'city': 'Pattaya', 'popularity2014': 6.4273, 'geometry': [12.9318593, 100.9006905], 'country': 'Thailand', 'popularity2013': 6.9862}, {'city': 'Shanghai', 'popularity2014': 6.3988, 'geometry': [31.2253441, 121.4888922], 'country': 'China', 'popularity2013': 6.1409}, {'city': 'Prague', 'popularity2014': 6.3461, 'geometry': [50.0874654, 14.4212503], 'country': 'Czech Republic', 'popularity2013': 6.2085}, {'city': 'Las Vegas', 'popularity2014': 6.1334, 'geometry': [36.1662859, -115.1492249], 'country': 'USA', 'popularity2013': 6.0469}, {'city': 'Mecca', 'popularity2014': 6.1206, 'geometry': [21.4227956, 39.8211631], 'country': 'Saudi', 'popularity2013': 5.7622}, {'city': 'Milan', 'popularity2014': 6.0501, 'geometry': [45.4667971, 9.1904984], 'country': 'Italy', 'popularity2013': 5.8739}, {'city': 'Tokyo', 'popularity2014': 5.9933, 'geometry': [35.6828378, 139.7589667], 'country': 'Japan', 'popularity2013': 4.6515}, {'city': 'Barcelona', 'popularity2014': 5.9728, 'geometry': [41.3825596, 2.1771353], 'country': 'Spain', 'popularity2013': 5.6993}, {'city': 'Amsterdam', 'popularity2014': 5.7141, 'geometry': [52.3745403, 4.8979755], 'country': 'Netherlands', 'popularity2013': 5.2041}, {'city': 'Vienna', 'popularity2014': 5.4221, 'geometry': [48.2083537, 16.3725042], 'country': 'Austria', 'popularity2013': 5.1876}, {'city': 'Los Angeles', 'popularity2014': 5.2726, 'geometry': [34.0543942, -118.2439408], 'country': 'USA', 'popularity2013': 5.154}, {'city': 'Venice', 'popularity2014': 5.237, 'geometry': [45.4371908, 12.3345899], 'country': 'Italy', 'popularity2013': 5.1596}, {'city': 'Moscow', 'popularity2014': 4.9762, 'geometry': [55.7506828, 37.6174976], 'country': 'Russia', 'popularity2013': 5.56}, {'city': 'Johannesburg', 'popularity2014': 4.7721, 'geometry': [-26.2049999, 28.0497222], 'country': 'South Africa', 'popularity2013': 4.5122}, {'city': 'Delhi', 'popularity2014': 4.6988, 'geometry': [28.6517178, 77.2219388], 'country': 'India', 'popularity2013': 3.6722}, {'city': 'Orlando', 'popularity2014': 4.6771, 'geometry': [28.5421175, -81.3790461], 'country': 'USA', 'popularity2013': 4.4838}, {'city': 'Berlin', 'popularity2014': 4.6746, 'geometry': [52.5170365, 13.3888599], 'country': 'Germany', 'popularity2013': 4.3465}, {'city': 'Mumbai', 'popularity2014': 4.5751, 'geometry': [18.9523804, 72.8327112], 'country': 'India', 'popularity2013': 3.6439}, {'city': 'Budapest', 'popularity2014': 4.452, 'geometry': [47.4983815, 19.0404707], 'country': 'Hungary', 'popularity2013': 4.222}, {'city': 'Ho Chi Minh City', 'popularity2014': 4.4, 'geometry': [10.7758439, 106.7017555], 'country': 'Vietnam', 'popularity2013': 4.1977}, {'city': 'Beijing', 'popularity2014': 4.3353, 'geometry': [39.9059631, 116.391248], 'country': 'China', 'popularity2013': 4.5013}, {'city': 'Florence', 'popularity2014': 4.2782, 'geometry': [43.7698712, 11.2555757], 'country': 'Italy', 'popularity2013': 4.1861}, {'city': 'Madrid', 'popularity2014': 4.1783, 'geometry': [40.4167047, -3.7035824], 'country': 'Spain', 'popularity2013': 4.0331}, {'city': 'Riyadh', 'popularity2014': 4.1727, 'geometry': [24.6319692, 46.7150648], 'country': 'Saudi Arabia', 'popularity2013': 3.9287}, {'city': 'Warsaw', 'popularity2014': 4.1578, 'geometry': [52.2319237, 21.0067265], 'country': 'Poland', 'popularity2013': 3.9787}, {'city': 'Cancun', 'popularity2014': 4.0025, 'geometry': [21.1708906, -86.8403164], 'country': 'Mexico', 'popularity2013': 2.8161}, {'city': 'Lima', 'popularity2014': 3.9479, 'geometry': [-12.0038889, -76.6388889], 'country': 'Peru', 'popularity2013': 4.944}, {'city': 'Nairobi', 'popularity2014': 3.9135, 'geometry': [-1.2832532, 36.8172449], 'country': 'Kenya', 'popularity2013': 3.687}, {'city': 'Chennai', 'popularity2014': 3.8579, 'geometry': [13.0796914, 80.2829533], 'country': 'India', 'popularity2013': 3.5812}, {'city': 'Johor Bahru', 'popularity2014': 3.6317, 'geometry': [1.4953041, 103.7550839], 'country': 'Malaysia', 'popularity2013': 3.0264}, {'city': 'Dublin', 'popularity2014': 3.5716, 'geometry': [53.3497645, -6.2602731], 'country': 'Ireland', 'popularity2013': 3.3983}, {'city': 'Denpasar', 'popularity2014': 3.5661, 'geometry': [-8.6524972, 115.2191175], 'country': 'Indonesia', 'popularity2013': 3.2419}, {'city': 'Athens', 'popularity2014': 3.3883, 'geometry': [37.9841493, 23.7279843], 'country': 'Greece', 'popularity2013': 2.618}, {'city': 'San Francisco', 'popularity2014': 3.3275, 'geometry': [37.7792808, -122.4192362], 'country': 'USA', 'popularity2013': 3.2527}, {'city': 'Agra', 'popularity2014': 3.2644, 'geometry': [27.1752554, 78.0098161], 'country': 'India', 'popularity2013': 2.4832}, {'city': 'Hangzhou', 'popularity2014': 3.2307, 'geometry': [30.2756741, 120.1504858], 'country': 'China', 'popularity2013': 3.1601}, {'city': 'Toronto', 'popularity2014': 3.2184, 'geometry': [43.6529206, -79.3849007], 'country': 'Canada', 'popularity2013': 3.0107}, {'city': 'Mugla', 'popularity2014': 3.1519, 'geometry': [37.2148161, 28.3644557], 'country': 'Turkey', 'popularity2013': 3.0627}, {'city': 'Sydney', 'popularity2014': 3.1485, 'geometry': [-33.8548156, 151.2164539], 'country': 'Australia', 'popularity2013': 2.953}, {'city': 'Brussels', 'popularity2014': 3.1, 'geometry': [50.8465565, 4.351697], 'country': 'Belgium', 'popularity2013': 2.99}, {'city': 'Jerusalem', 'popularity2014': 3.0947, 'geometry': [31.7791134, 35.2266286], 'country': 'Israel', 'popularity2013': 3.0571}, {'city': 'Edirne', 'popularity2014': 3.0909, 'geometry': [41.6759327, 26.5587225], 'country': 'Turkey', 'popularity2013': 2.8405}, {'city': 'Munich', 'popularity2014': 3.0268, 'geometry': [48.1371079, 11.5753822], 'country': 'Germany', 'popularity2013': 2.8899}, {'city': 'Hanoi', 'popularity2014': 3.0, 'geometry': [21.0292095, 105.85247], 'country': 'Vietnam', 'popularity2013': 2.3091}, {'city': 'Jaipur', 'popularity2014': 2.9676, 'geometry': [26.9161293, 75.8204056], 'country': 'India', 'popularity2013': 2.403}, {'city': 'Zhuhai', 'popularity2014': 2.9192, 'geometry': [22.2657516, 113.568045], 'country': 'China', 'popularity2013': 2.6323}, {'city': 'Punta Cana', 'popularity2014': 2.886, 'geometry': [18.58182, -68.40431], 'country': 'Dominican Republic', 'popularity2013': 2.5912}, {'city': 'Cairo', 'popularity2014': 2.772, 'geometry': [30.0488185, 31.2436663], 'country': 'Egypt', 'popularity2013': 2.625}, {'city': 'Buenos Aires', 'popularity2014': 2.7525, 'geometry': [-34.6143904, -58.4460878], 'country': 'Argentina', 'popularity2013': 2.6931}, {'city': 'Sofia', 'popularity2014': 2.6907, 'geometry': [42.6977211, 23.3225964], 'country': 'Bulgaria', 'popularity2013': 2.534}, {'city': 'St Petersburg', 'popularity2014': 2.6711, 'geometry': [59.9393657, 30.3153628], 'country': 'Russia', 'popularity2013': 3.1059}, {'city': 'Chiang Mai', 'popularity2014': 2.6587, 'geometry': [18.7905618, 98.9880909], 'country': 'Thailand', 'popularity2013': 2.127}, {'city': 'Lisbon', 'popularity2014': 2.6265, 'geometry': [38.7077926, -9.1365069], 'country': 'Portugal', 'popularity2013': 2.329}, {'city': 'Heraklion', 'popularity2014': 2.606, 'geometry': [35.3340134, 25.1328543], 'country': 'Greece', 'popularity2013': 2.472}, {'city': 'Halong', 'popularity2014': 2.6, 'geometry': [20.9528365, 107.0800003], 'country': 'Vietnam', 'popularity2013': 2.604}, {'city': 'Pulau Pinang', 'popularity2014': 2.593, 'geometry': [5.7396727, 103.0026634], 'country': 'Malaysia', 'popularity2013': 2.3724}, {'city': 'Mexico City', 'popularity2014': 2.5885, 'geometry': [19.4325301, -99.1332101], 'country': 'Mexico', 'popularity2013': 2.3787}, {'city': 'Siem Reap', 'popularity2014': 2.5844, 'geometry': [13.3452447, 103.8541628], 'country': 'Cambodia', 'popularity2013': 2.4244}, {'city': 'Jerba', 'popularity2014': 2.55, 'geometry': [33.8, 10.9], 'country': 'Tunisia', 'popularity2013': 2.5384}, {'city': 'Cracow', 'popularity2014': 2.5, 'geometry': [50.0619474, 19.9368564], 'country': 'Poland', 'popularity2013': 2.45}, {'city': 'Jakarta', 'popularity2014': 2.4787, 'geometry': [-6.1753941, 106.8271826], 'country': 'Indonesia', 'popularity2013': 2.3057}, {'city': 'Rio de Janeiro', 'popularity2014': 2.4445, 'geometry': [-22.9110136, -43.2093726], 'country': 'Brazil', 'popularity2013': 1.6673}, {'city': 'Honolulu', 'popularity2014': 2.4315, 'geometry': [21.304547, -157.8556763], 'country': 'USA', 'popularity2013': 2.3697}, {'city': 'Manama', 'popularity2014': 2.4212, 'geometry': [26.2235041, 50.5822436], 'country': 'Bahrain', 'popularity2013': 2.349}, {'city': 'Andorra la Vella', 'popularity2014': 2.3979, 'geometry': [42.5069391, 1.5212467], 'country': 'Andorra', 'popularity2013': 2.3281}, {'city': 'Zurich', 'popularity2014': 2.3721, 'geometry': [47.3685586, 8.5404434], 'country': 'Switzerland', 'popularity2013': 2.2591}, {'city': 'Tel Aviv', 'popularity2014': 2.3699, 'geometry': [32.0804808, 34.7805274], 'country': 'Israel', 'popularity2013': 2.3411}, {'city': 'Marrakech', 'popularity2014': 2.2305, 'geometry': [31.6259901, -7.9886082], 'country': 'Morocco', 'popularity2013': 2.1191}, {'city': 'Nice', 'popularity2014': 2.1846, 'geometry': [43.7009358, 7.2683912], 'country': 'France', 'popularity2013': 2.2638}, {'city': 'Vancouver', 'popularity2014': 2.1755, 'geometry': [49.2608944, -123.1139382], 'country': 'Canada', 'popularity2013': 2.0181}, {'city': 'Sharm el Sheikh', 'popularity2014': 2.1606, 'geometry': [27.8669082, 34.3014551], 'country': 'Egypt', 'popularity2013': 2.046}, {'city': 'Melbourne', 'popularity2014': 2.1281, 'geometry': [-37.8142175, 144.9631608], 'country': 'Australia', 'popularity2013': 1.929}, {'city': 'Guilin', 'popularity2014': 2.03, 'geometry': [25.276006, 110.2847529], 'country': 'China', 'popularity2013': 1.94}, {'city': 'Frankfurt', 'popularity2014': 2.0167, 'geometry': [50.1106529, 8.6820934], 'country': 'Germany', 'popularity2013': 1.965}, {'city': 'Amman', 'popularity2014': 1.9774, 'geometry': [31.9515694, 35.9239625], 'country': 'Jordan', 'popularity2013': 1.8725}, {'city': 'Jeju', 'popularity2014': 1.9407, 'geometry': [33.4930566, 126.5130589], 'country': 'South Korea', 'popularity2013': 1.7792}, {'city': 'Rhodes', 'popularity2014': 1.931, 'geometry': [36.4379874, 28.2233083], 'country': 'Greece', 'popularity2013': 1.785}, {'city': 'Marne-La-Vallee', 'popularity2014': 1.9294, 'geometry': [47.9824502, 0.2135632], 'country': 'France', 'popularity2013': 2.0014}, {'city': 'Samui', 'popularity2014': 1.8562, 'geometry': [9.5013528, 99.9956715], 'country': 'Thailand', 'popularity2013': 1.6141}, {'city': 'Kolkatta', 'popularity2014': 1.8548, 'geometry': [22.5722242, 88.3614363], 'country': 'India', 'popularity2013': 1.6717}, {'city': 'Taichung', 'popularity2014': 1.8355, 'geometry': [24.163162, 120.6478282], 'country': 'Taiwan', 'popularity2013': 1.4119}, {'city': 'Artvin', 'popularity2014': 1.8271, 'country': 'Turkey', 'popularity2013': 1.7327}]

import json
import os
import random
import requests
import json

def rand(l, r):
    return int(random.random() * (r - l) + l)

def rand01():
    return random.random()

def valid(x, l, r):
    return max(l, min(r, x))

def gen_whe(tempLikely, tempStability, windLikely, windStability, cloudyLikely, cloudyStability):
    whe = {}
    likelyTempChange = 15 * (1 - tempStability)
    likelyMinTemp = tempLikely - likelyTempChange
    likelyMaxTemp = tempLikely + likelyTempChange
    temp = rand(likelyMinTemp, likelyMaxTemp)
    whe["temp"] = valid(temp, -10, 40)
    likelyCloudyChange = 50 * (1 - cloudyStability)
    likelyMinCloudy = cloudyLikely - likelyCloudyChange
    likelyMaxCloudy = cloudyLikely + likelyCloudyChange
    cloudy = rand(likelyMinCloudy, likelyMaxCloudy)
    whe["cloudy"] = valid(cloudy, 0, 100)
    likelyWindChange = 15 * (1 - windStability)
    likelyMinWind = windLikely - likelyWindChange
    likelyMaxWind = windLikely + likelyWindChange
    wind =  rand(likelyMinWind, likelyMaxWind)
    whe["wind"] = valid(wind, 0, 35)
    return whe

def get_coord(city, con):
    url = 'https://api.opencagedata.com/geocode/v1/json?q='+city+' '+con+'&key=d5183ae8f7faa88604a270aaa5a04b39';
    str = requests.get(url).text
    parsed = json.loads(str)
    for res in parsed['results']:
        if ('components' in res and '_type' in res['components'] and res['components']['_type'] == 'city'):
            print([res['geometry']['lat'], res['geometry']['lng']])
            return [res['geometry']['lat'], res['geometry']['lng']]
    for res in parsed['results']:
        if ('components' in res and '_type' in res['components'] and res['components']['_type'] == 'state'):
            print([res['geometry']['lat'], res['geometry']['lng']])
            return [res['geometry']['lat'], res['geometry']['lng']]
    res = parsed['results'][0]
    print(res['geometry']['lat'], res['geometry']['lng'])
    return [res['geometry']['lat'], res['geometry']['lng']]

slash = '/'.replace('q', '')
ans = []

def gen_all():
    for i in range(0, 99, 1):
        cit = data[i]
        ltemp = rand(10, 30)
        temps = rand01()
        lwind = rand(0, 25)
        winds = rand01()
        lcloudy = rand(0, 100)
        cloudys = rand01()
        month_whe = []
        for d in range(1, 32, 1):
            day_whe = gen_whe(ltemp, temps, lwind, winds, lcloudy, cloudys)
            month_whe.append(day_whe)
        #coord = get_coord(cit["city"], cit["country"])
        #data[i]['geometry'] = coord
        coord = data[i]['geometry']
        print(coord)
        ans.append({
            "country_name": cit["country"],
            "city_name": cit["city"],
            "wheather": month_whe,
            "latt": coord[0],
            "long": coord[1]
        })
        #print(str(i + 1) + '%')
gen_all()
print(ans)