var ExtractTextPlugin = require("extract-text-webpack-plugin");
var path = require('path');
module.exports = {
    entry: './js/init.js',
    output: {
        path: __dirname,
        filename: 'bundle.js'
    },
    module: {
        prevLoaders: [
            {
                test: /\.css$/,
                loader: 'css-loader!csso-loader',
            }
        ],
        postLoaders: [
            {
                test: /\jsx?$/,
                loader: "uglify"
            }
        ],
        loaders: [
            {
                test: /\jsx?$/,
                loader: 'babel',
                query: {
                    presets: ['es2015']
                }
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract("css-loader")
            },
            {
                test: /\.png$/, loader: "file-loader"
            }
        ]
    },
  csso: {
    debug: 3
  },
    plugins: [
        new ExtractTextPlugin("styles.css", { allChunks: true })
    ]
};